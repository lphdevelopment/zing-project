package com.why.zing.common.exception;

import lombok.Getter;

/**
 * 异常父类
 *
 * @author 睁眼看世界
 * @date 2018/11/11
 */

@Getter
public class ZingException extends RuntimeException {

  private final String code;
  private final String info;

  public ZingException(ExceptionCode exceptionCode) {
    super(exceptionCode.getInfo());
    this.code = exceptionCode.getCode();
    this.info = exceptionCode.getInfo();
  }

  public ZingException(String code, String info) {
    super(info);
    this.code = code;
    this.info = info;
  }
}
