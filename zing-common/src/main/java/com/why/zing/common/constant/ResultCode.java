package com.why.zing.common.constant;

import com.why.zing.common.exception.ExceptionCode;

/**
 * 返回码定义
 *
 * @author why
 * @date 2019年4月19日
 */
public enum ResultCode implements ExceptionCode {

    // 请求成功
    SUCCESS("200", "成功"),

    // 服务器内部错误
    ERROR("500", "服务异常");

    private final String code;
    private final String info;

    ResultCode(String value, String info) {
        this.code = value;
        this.info = info;
    }


    /**
     * 获取异常编码
     *
     * @return 异常码
     */
    @Override
    public String getCode() {
        return code;
    }

    /**
     * 获取异常信息
     *
     * @return 异常信息
     */
    @Override
    public String getInfo() {
        return info;
    }
}
