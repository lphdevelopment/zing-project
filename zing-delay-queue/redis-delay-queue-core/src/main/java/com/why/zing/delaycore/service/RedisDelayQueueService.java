package com.why.zing.delaycore.service;

import com.why.zing.delaycore.model.Job;
import com.why.zing.delaycore.model.JobDie;

/**
 * 提供给外部服务的操作接口
 *
 * @author why
 * @date 2020年1月15日
 */
public interface RedisDelayQueueService {

    /**
     * 添加job元信息
     *
     * @param job 元信息
     */
    void addJob(Job job);


    /**
     * 删除job信息
     *
     * @param job
     */
    void deleteJob(JobDie jobDie);
}
