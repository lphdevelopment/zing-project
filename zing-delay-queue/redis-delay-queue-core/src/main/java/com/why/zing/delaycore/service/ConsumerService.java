package com.why.zing.delaycore.service;


/**
 * 消费者类，对ReadyQueue中的消息进行消费
 *
 * @author 睁眼看世界
 * @date 2020年1月16日
 */
public interface ConsumerService {

    /**
     * 消费ReadyQueue的消息
     * @param url 请求URL
     * @param body 消息内容
     * @return 执行结果
     */
    Boolean consumerMessage(String url, String body);
}
