package com.why.zing.delaycore;

import com.why.zing.delaycore.constant.RedisQueueKey;
import com.why.zing.delaycore.model.Job;
import org.junit.jupiter.api.Test;
import org.redisson.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@SpringBootTest(classes = RedisDelayQueueCoreApplication.class)
public class RedisDelayQueueCoreApplicationTests {

    @Autowired
    private RedissonClient redissonClient;





    @Test
    public void testBlockQueue() {
        RBlockingQueue<String> queue = redissonClient.getBlockingQueue("anyQueue");
        queue.offer("why");
        System.out.println(queue.peek());
        System.out.println(queue.poll());
    }


    @Test
    public void testPopBlockQueue() throws InterruptedException {
        RBlockingQueue<String> queue = redissonClient.getBlockingQueue("anyQueue");
        System.out.println("等待着弹出信息");
        String poll = queue.poll(Integer.MAX_VALUE, TimeUnit.DAYS);
        System.out.println(poll);
    }


    @Test
    public void testScoreSort() {
        RScoredSortedSet<Object> sortedSet = redissonClient.getScoredSortedSet("scoreSort2");
        sortedSet.add(1000, "why");
        sortedSet.add(2000, "tom");
        sortedSet.add(10000, "zing");
        sortedSet.add(6000, "kitty");
        sortedSet.add(3000, "kawen");
        sortedSet.add(4000, "hhdf");
        sortedSet.add(5000, "lllll");


        RScoredSortedSet<Object> sortedSet2 = redissonClient.getScoredSortedSet("scoreSort");
        System.out.println(sortedSet2.pollFirst());
        System.out.println(sortedSet2.pollLast());


        RScoredSortedSet<Object> sortedSet3 = redissonClient.getScoredSortedSet("scoreSort2");
        Double score = sortedSet3.getScore("kitty"); // 获取元素的评分
        System.out.println(score);

        Collection<Object> objects = sortedSet3.valueRange(1000, true, 4000, true);
        Object[] objects1 = objects.toArray();
        for (int i = 0; i < objects.size(); i++) {
            System.out.println(objects1[i]);
        }

    }


    @Test
    public void testRedis() {
        RBucket<String> cashier = redissonClient.getBucket("cashier");
        cashier.set("王洪玉");

        RBucket<String> cashier1 = redissonClient.getBucket("cashier");
        String s = cashier1.get();
        System.out.println(s);
    }

    @Test
    public void testSortSet() {
        Job job = new Job();
        job.setJobId(UUID.randomUUID().toString());
//        job.setJobId("4428fca8-8143-43d9-aade-16b68546936d");
        job.setTopic("CallBack");
        job.setDelay(1000L);
        job.setBody("182371283792173921873");

        String topicId = RedisQueueKey.getTopicId(job.getTopic(), job.getJobId());

        // 1. 将job添加到 JobPool中
        RMap<String, Job> jobPool = redissonClient.getMap(RedisQueueKey.JOB_POOL_KEY);
        jobPool.put(topicId, job);

        // 2. 将job添加到 DelayBucket中
        RScoredSortedSet<Object> delayBucket = redissonClient.getScoredSortedSet(RedisQueueKey.RD_ZSET_BUCKET_PRE);
        delayBucket.add(job.getDelay(), topicId);


        String topicId1 = RedisQueueKey.getTopicId(job.getTopic(), job.getJobId());

        RMap<String, Job> jobPool1 = redissonClient.getMap(RedisQueueKey.JOB_POOL_KEY);
        jobPool1.remove(topicId1);

        RScoredSortedSet<Object> delayBucket1 = redissonClient.getScoredSortedSet(RedisQueueKey.RD_ZSET_BUCKET_PRE);
        delayBucket1.remove(topicId1);

    }

}
